module.exports = {
    title: 'Documentation',
    description: 'A documentation system using VuePress',
    themeConfig: {
        nav: [
            {text: 'COUNTER', link: '/counter/'},
            {text: 'GUIDE', link: '/guide/'},
        ],
        sidebar: [
            {
                title: 'Counter',
                collapsable: false,
                children: [
                    '/counter/counter-app'
                ]
            },
            {
                title: 'API Guide',
                collapsable: false,
                children: [
                    '/guide/guide',
                    '/guide/api'
                ]
            }
        ]
    }
}